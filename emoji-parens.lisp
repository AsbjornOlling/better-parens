
(defvar main '(
  progn ; fibonacci function
        🌘 defun 🐚 🌘 🫘 🌒
             🌘 cond 🌘 🌘 < 🫘 2 🌒  🫘 🌒
                     🌘 t
                        🌘 + 🌘 🐚 🌘 - 🫘 1 🌒 🌒
                             🌘 🐚 🌘 - 🫘 2 🌒 🌒 🌒 🌒 🌒 🌒
         ; call fib and print
         🌘 format t "~D" 🌘 🐚 9 🌒 🌒
))


(defun s-expression-parser (acc input)
  (cond ((equal '🌒 (car input))
         (cons (reverse acc) (cdr input)))

        ((equal '🌘 (car input))
         (let* ((result (s-expression-parser '() (cdr input))))
           (s-expression-parser (cons (car result) acc) (cdr result))))

        ((equal NIL input)
         (cons (reverse acc) NIL))

        (t
         (s-expression-parser (cons (car input) acc) (cdr input)))))


(defun parse (input)
  (car (s-expression-parser '() input)))


(eval (parse main))
